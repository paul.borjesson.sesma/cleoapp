package com.example.cleoapp;

import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cleoapp.databinding.FragmentListBinding;
import com.example.cleoapp.databinding.RowBinding;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class FragmentList extends AppFragment {

    private FragmentListBinding binding;
    private Uri uriImagen;
    private ImagesAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentListBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.menu_singles, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.item1:
                //DELETE
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.previsualizacion.setOnClickListener(v -> galeria.launch("image/*"));

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            if (uri != null) {
                Glide.with(this).load(uri).into(binding.previsualizacion);
                uriImagen = uri;
            }
        });

        binding.btnUpload.setOnClickListener( v -> {
            binding.btnUpload.setEnabled(false);

            FirebaseStorage.getInstance()
                .getReference("/images/" + UUID.randomUUID() + ".jpg")
                .putFile(uriImagen)
                .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl())
                .addOnSuccessListener(uri -> {
                    Image image = new Image();
                    image.imageId = uri.toString();
                    image.self = db.collection("images").document().getId();
                    db.collection("images").document(image.self).set(image).addOnSuccessListener(task -> {
                        appViewModel.setUriImagenSeleccionada(null);
                    });
                    binding.btnUpload.setEnabled(true);
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(getContext(), "Error, show to Aze: " + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    binding.btnUpload.setEnabled(true);
                });
        });

        binding.recView.setAdapter(adapter = new ImagesAdapter());
        binding.recView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        db.collection("images").addSnapshotListener((queryDocumentSnapshots, e) -> {
            List<Image> newImages = new ArrayList<>();
            if (queryDocumentSnapshots != null) {
                for (DocumentSnapshot doc : queryDocumentSnapshots) {
                    Image image = doc.toObject(Image.class);
                    if (image != null) {
                        newImages.add(image);
                    }
                }
            }
            adapter.updateImages(newImages);
        });



    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
        private final List<Image> images = new ArrayList<>();

        @NonNull
        @Override
        public ImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(RowBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ImagesAdapter.ViewHolder holder, int position) {
            Glide.with(requireContext()).load(images.get(position).imageId).into(holder.binding.imageView);
        }

        @Override
        public int getItemCount() { return images.size(); }

        public void updateImages(List<Image> newList) {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return images.size();
                }

                @Override
                public int getNewListSize() {
                    return newList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return images.get(oldItemPosition).imageId.equals(newList.get(newItemPosition).imageId);
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Image oldImg = images.get(oldItemPosition);
                    Image newImg = newList.get(newItemPosition);
                    return !Objects.equals(oldImg.imageId, newImg.imageId);
                }
            });

            images.clear();
            images.addAll(newList);
            diffResult.dispatchUpdatesTo(this);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RowBinding binding;
            public ViewHolder(RowBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }

}